## What is this?

This is a python restful api that will do a ARIMA curve estimate. Since this is long calculation stuff, I wrapped the functionality around the multiprocessing lib so all your cpu cores should spike. When completed, the backend will show a prediction vs test curve, and best results will be sent to you. The curve deviation is judge by mean-square-error.