import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.tsa.arima_model import ARIMA
import itertools
import warnings
import sys
warnings.filterwarnings('ignore')
from sklearn.metrics import mean_squared_error
import multiprocessing

from flask import Flask, request, jsonify
from flask_cors import CORS
import json


plot_debugging = False
app = Flask(__name__)
CORS(app, resources={r"/api/*": {"origins": "*"}})
prediction_lock = False

#taskkill /F /IM python.exe /T

sales = pd.read_csv('sales-cars.csv')

allValues = sales['Sales'].values

# print('ALL VALUE ', allValues)

train = allValues[0:27]
test = allValues[26:]

def getPermutations(low, high):
    p=d=q=range(low,high)
    pdq = list(itertools.product(p,d,q))
    return pdq

def predicts(train, order, numberOfPredictions):
    try:
        print('ARIMA START: ', order)
        model_arima = ARIMA(train,order=order) #order should be like (1,1,1)
        model_arima_fit = model_arima.fit(disp=0)
        # print(model_arima_fit.aic) #alterative to mse, this should be low
        prediction= model_arima_fit.forecast(steps=numberOfPredictions)[0]
        result = {}
        result['param'] = order
        result['prediction'] = prediction
        print('ARIMA END: ', order)
        return result
    except Exception as e:
        print('EXCEPTION HAS OCCURED! ', order)
        return None

#use this when you have a good order
def predictOne(train, order, numberOfPredictions):
    model_arima = ARIMA(train,order=order)
    model_arima_fit = model_arima.fit(disp=0)
    prediction= model_arima_fit.forecast(steps=numberOfPredictions)[0]
    return prediction

def plotFast(red, black):
    plt.plot(black)
    plt.plot(red,color='red')
    plt.show(block=False)
    plt.pause(0.2)
    plt.close()

def plot(red, black):
    plt.plot(black)
    plt.plot(red,color='red')
    plt.show()


class Predictor:
    lowPDQ = None
    lowMSE = None
    def __init__(self, train, test, iterations, skipIteration=1):
        self.test = test
        self.train = train
        self.iterations = iterations
        self.skipIteration = skipIteration
        return
    def onResult(self, result):
        if not result:
            return
        mse = mean_squared_error(self.test, result['prediction'])
        if not self.lowMSE:
            self.lowMSE = mse
        if mse < self.lowMSE:
            self.lowMSE = mse 
            self.lowPDQ = result['param']
            print('Current best PDQ/MSE ', self.lowPDQ, self.lowMSE)
            
    def predictor(self):
        print('ARIMA iterations: ', self.iterations)
        pool = multiprocessing.Pool()    
        for param in getPermutations(self.skipIteration,self.iterations):
            pool.apply_async(predicts, args=(self.train, param, len(self.test)), callback=self.onResult)
        pool.close()
        pool.join()


@app.route('/api/predict', methods=['POST', 'GET'])
def api_predict():
    global prediction_lock
    if prediction_lock:
        print('BUSY!')
        return {'status': 'busy'}
    else:
        prediction_lock = True

    if request.method == 'POST':
        payload = request.json
        testPercent = payload['testPercent']/100

        #optional
        skipIteration = 1
        if payload['skipIteration']:
            skipIteration = payload['skipIteration']
            print('skipIteration ', skipIteration)


        testSlice = payload['train'][int(len(payload['train']) * (1-testPercent)) : ]
        trainSlice = payload['train'][:len(payload['train'])-len(testSlice)+1]
        print('train ', trainSlice)
        print('test ', testSlice)
        p = Predictor(trainSlice, testSlice, payload['iterations'], skipIteration)
        p.predictor()
        prediction_lock = False
    if not p.lowPDQ:
        print('TRAINING INSUFFICIENT')
        return {'status': 'none'}
    future = predictOne(trainSlice, p.lowPDQ, len(testSlice) + 2)
    predictionValue = future[len(future)-1]
    if plot_debugging:
        print(predictionValue)
        plot(future, testSlice)

    return jsonify({'mean_square_error': p.lowMSE, 'train': trainSlice, 'test': testSlice, 'predictionData': list(future), 'predictionValue': predictionValue, 'status': 'ok'})

@app.route('/api/graph', methods=['POST', 'GET'])
def api_graph():
    if request.method == 'POST':
        payload = request.json
        plot(payload['data'], payload['data'])
    return 'OK'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8090, debug=True)
    # p = Predictor(train, test, 9)
    # p.predictor()

    # future = predictOne(train, p.lowPDQ, len(test) + 1)
    # print(future[len(future)-1])
    # plot(future, test)

